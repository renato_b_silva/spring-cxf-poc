package br.com.bmg;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

import org.apache.cxf.transport.servlet.CXFServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePropertySource;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;

public class WebAppInitializer implements WebApplicationInitializer {

	private final Logger log = LoggerFactory.getLogger(WebAppInitializer.class);

	@Override
	public void onStartup(ServletContext container) {
		// Create the 'root' Spring application context
		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
		rootContext.register(AppConfiguration.class);

		// Manage the lifecycle of the root application context
		container.addListener(new ContextLoaderListener(rootContext));

		// Register and map the servlet
		initCxfServlet(container);

		//
		configureEnvironment(rootContext);
		initWebserviceLogging(rootContext.getEnvironment());
	}

	private void initCxfServlet(ServletContext container) {
		ServletRegistration.Dynamic cxfServlet = container.addServlet("cxfServlet", new CXFServlet());
		cxfServlet.setLoadOnStartup(1);
		cxfServlet.addMapping("/*");
	}

	private void configureEnvironment(AnnotationConfigWebApplicationContext context) {
		ConfigurableEnvironment env = context.getEnvironment();

		// profile
		if (env.getActiveProfiles().length == 0) {
			log.warn("Nenhum profile ativo. Usando [{}]", Constants.SPRING_PROFILE_LOCALHOST);
			env.setActiveProfiles(Constants.SPRING_PROFILE_LOCALHOST);
		}

		// configure propertySource
		try {
			Resource resource = context.getResource("classpath:/config/application.properties");
			env.getPropertySources().addLast(new ResourcePropertySource(resource));
			for (String profile : env.getActiveProfiles()) {
				String location = "classpath:/config/application-" + profile + ".properties";
				resource = context.getResource(location);
				env.getPropertySources().addLast(new ResourcePropertySource(resource));
			}
		} catch (IOException e) {
			log.error("Erro ao iniciar a aplicação.", e);
		}
	}
	
	private void initWebserviceLogging(ConfigurableEnvironment env) {
		// -Dcom.sun.xml.ws.transport.http.client.HttpTransportPipe.dump=true
		// -Dcom.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump=true
		// -Dcom.sun.xml.ws.transport.http.HttpAdapter.dump=true
		// -Dcom.sun.xml.internal.ws.transport.http.HttpAdapter.dump=true
		System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");

		if (env.acceptsProfiles(Constants.SPRING_PROFILE_PRODUCTION)) {
			LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
			context.getLogger("org.apache.cxf.services").setLevel(Level.valueOf("WARN"));
		}
	}
}