package br.com.bmg.ws;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlTransient;

@XmlTransient
public abstract class AbstractResult implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String DEFAULT_ERROR_MESSAGE = "Erro interno.";
	
	private StatusProcessamentoEnum status;

	private String mensagem;

	public AbstractResult() {
		super();
	}

	public AbstractResult(StatusProcessamentoEnum status) {
		this.status = status;
	}

	public AbstractResult(StatusProcessamentoEnum status, String mensagem) {
		this.status = status;
		this.mensagem = mensagem;
	}

	public StatusProcessamentoEnum getStatus() {
		return status;
	}

	public void setStatus(StatusProcessamentoEnum status) {
		this.status = status;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
}
