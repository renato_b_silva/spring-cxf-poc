package br.com.bmg.ws.helloword;

import javax.inject.Inject;
import javax.jws.WebService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;


@WebService(endpointInterface = "br.com.bmg.ws.helloword.HelloWordWS", name = "HelloWordWS")
public class HelloWordWSImpl implements HelloWordWS {

	private final Logger log = LoggerFactory.getLogger(HelloWordWSImpl.class);
	
	@Inject
	private Environment env;
	
	@Override
	public HelloWordResponse executar(HelloWordParameter param)  {
		log.info("Chamando metodo");
		HelloWordResponse response = new HelloWordResponse();
		response.setMensagemTratada("Hello Word: "+param.getMensagem()+" em:"+env.getProperty("spring.teste"));
		return response;
	}

}
