package br.com.bmg.ws.helloword;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import br.com.bmg.ws.AbstractResult;
import br.com.bmg.ws.StatusProcessamentoEnum;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"mensagemTratada", "mensagem", "status"})
public class HelloWordResponse extends AbstractResult{
	private static final long serialVersionUID = 1L;
	
	private String mensagemTratada;
	
	public HelloWordResponse() {
		super(StatusProcessamentoEnum.Sucesso);
	}
	
	public String getMensagemTratada() {
		return mensagemTratada;
	}
	public void setMensagemTratada(String mensagemTratada) {
		this.mensagemTratada = mensagemTratada;
	}
	
}
