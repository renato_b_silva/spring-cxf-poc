package br.com.bmg.ws.helloword;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

@WebService(name = "helloWordWS")
public interface HelloWordWS {

	public HelloWordResponse executar(@WebParam(name = "param") @XmlElement(required = true) HelloWordParameter param);

}
