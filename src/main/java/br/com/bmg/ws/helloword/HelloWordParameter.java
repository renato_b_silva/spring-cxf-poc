package br.com.bmg.ws.helloword;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import br.com.bmg.ws.AbstractParameter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "mensagem"})
public class HelloWordParameter extends AbstractParameter {
	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = true)
	private String mensagem;
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	public String getMensagem() {
		return mensagem;
	}
}
