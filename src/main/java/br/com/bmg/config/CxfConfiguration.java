package br.com.bmg.config;

import javax.inject.Inject;
import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import br.com.bmg.ws.helloword.HelloWordWSImpl;

@Configuration
@ImportResource({"classpath:META-INF/cxf/cxf.xml", "classpath:META-INF/cxf/cxf-servlet.xml"})
public class CxfConfiguration {

	@Inject
	private Bus bus;

	@Bean
	public HelloWordWSImpl heloWordWSService() {
		return new HelloWordWSImpl();
	}

	@Bean
	public Endpoint heloWordServiceEndpoint() {
		EndpointImpl endpoint = new EndpointImpl(bus, heloWordWSService());
		endpoint.publish("/HelloWord");
		return endpoint;
	}

	
}
