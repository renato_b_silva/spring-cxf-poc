package br.com.bmg;

public final class Constants {

	public static final String SPRING_PROFILE_LOCALHOST = "local";
	public static final String SPRING_PROFILE_DEVELOPMENT = "dev";
	public static final String SPRING_PROFILE_HOMOLOGATION = "hml";
	public static final String SPRING_PROFILE_PRODUCTION = "prod";
	public static final String SPRING_PROFILE_INTEGRATION = "integ";
	
}
